# Esto es un titulo

Este es mi primer repositorio en GitLab. Me llamo **Jon Gonzalez Pallares**.

Para poner links [google](google.com)

Como poner *Letra cursiva*

> Para crear cajas de texto y 
  poner en mas de una linea

Lista ordenada
1. Primero 
2. Segundo 
3. Tercero

Lista no ordenada
- Elemento uno
- Elemento dos
- Elemento tres

Para introducir codigo `codigo`


Linea horizontal para separar contenido
---

Para poner imagenes: ![Jim Hawkins](Imagenes/JimHawkins.png)

---

Para crear tablas
|Primer campo|Segundo campo|
|------|-------|
|Cabezera|Cuerpo|
|Cabezera|Cuerpo|


Cuerpos de codigo
```
{
    "primer nombre": "Jon", 
    "primer apellido": "Gonzalez",
    "segundo apellido": "Pallares"
}
```